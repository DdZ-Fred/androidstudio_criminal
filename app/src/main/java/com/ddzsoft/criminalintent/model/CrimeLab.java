package com.ddzsoft.criminalintent.model;

import android.content.Context;
import android.util.Log;

import com.ddzsoft.criminalintent.datastore.CriminalIntentJSONSerializer;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Frédéric on 10/07/2014.
 */
public class CrimeLab {

    private static final String TAG = "CrimeLab";
    private static final String FILENAME = "crimes.json";

    private static CrimeLab sCrimeLab;
    private Context mAppContext;
    private ArrayList<Crime> mCrimes;
    private CriminalIntentJSONSerializer mSerializer;

    private CrimeLab(Context appCtx){
        mAppContext = appCtx;
        mSerializer = new CriminalIntentJSONSerializer(mAppContext, FILENAME);

        try {
            mCrimes = mSerializer.loadCrimesFromExternalStorage();
        } catch (Exception e) {
            e.printStackTrace();
            mCrimes = new ArrayList<Crime>();
            Log.d(TAG, "Error loading crimes: ", e);
        }


    }

    public static CrimeLab get(Context c){
        if(sCrimeLab == null){
            sCrimeLab = new CrimeLab(c.getApplicationContext());
        }
        return sCrimeLab;
    }

    public ArrayList<Crime> getCrimes() {
        return mCrimes;
    }

    public Crime getCrime(UUID id){
        for(Crime c : mCrimes){
            if(c.getId().equals(id)){
                Log.d("CrimeLab", "getCrime() called");
                return c;
            }
        }

        return null;
    }

    public void addCrime(Crime c){
        mCrimes.add(c);
    }

    public void deleteCrime(Crime c){
        mCrimes.remove(c);
    }

    public boolean saveCrimes() throws IOException, JSONException {
        try {
            mSerializer.saveCrimes(mCrimes);
            Log.d(TAG, "Crimes saved to file");
            return true;
        } catch (Exception e) {
            Log.d(TAG, "Error saving crimes: ", e);
            return false;
        }
    }

    public boolean saveCrimesInExternalStorage() throws IOException, JSONException {

        try {
            mSerializer.saveCrimesInExternalStorage(mCrimes);
            Log.d(TAG, "Crimes saved in external storage");
            return true;
        } catch (Exception e) {
            Log.d(TAG, "Error saving crimes on external storage ");
            return false;
        }

    }

}

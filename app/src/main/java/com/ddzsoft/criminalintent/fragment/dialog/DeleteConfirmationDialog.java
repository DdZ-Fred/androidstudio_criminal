package com.ddzsoft.criminalintent.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;

import com.ddzsoft.criminalintent.R;
import com.ddzsoft.criminalintent.fragment.CrimeFragment;
import com.ddzsoft.criminalintent.model.Crime;
import com.ddzsoft.criminalintent.model.CrimeLab;

import java.util.UUID;

/**
 * Created by Frédéric on 18/07/2014.
 */
public class DeleteConfirmationDialog extends DialogFragment {

    private Crime mCrime;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UUID crimeId = (UUID) getArguments().getSerializable(CrimeFragment.EXTRA_CRIME_ID);
        mCrime = CrimeLab.get(getActivity()).getCrime(crimeId);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_crime_confirmation)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        CrimeLab.get(getActivity()).deleteCrime(mCrime);

                        dialog.dismiss();

                        if(NavUtils.getParentActivityName(getActivity()) != null){
                            NavUtils.navigateUpFromSameTask(getActivity());
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }


    @Override
    public void onPause() {
        super.onPause();
//        Come back the CrimeListActivity
/*        if(NavUtils.getParentActivityName(getActivity()) != null){
            NavUtils.navigateUpFromSameTask(getActivity());
        }*/
    }

    public static DeleteConfirmationDialog newInstance(UUID crimeId){
        Bundle args = new Bundle();
        args.putSerializable(CrimeFragment.EXTRA_CRIME_ID, crimeId);
        DeleteConfirmationDialog dialog = new DeleteConfirmationDialog();
        dialog.setArguments(args);
        return dialog;
    }

}
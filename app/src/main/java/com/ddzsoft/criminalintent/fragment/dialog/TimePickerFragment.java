package com.ddzsoft.criminalintent.fragment.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TimePicker;

import com.ddzsoft.criminalintent.R;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Frédéric on 11/07/2014.
 */
public class TimePickerFragment extends DialogFragment {

    private Date mDate;
    private Calendar mCalendar;



    private void sendResult(int resultCode){
        if(getTargetFragment() == null){
            return;
        }

        Intent i = new Intent();
        i.putExtra(ChooserFragment.EXTRA_DATE, mDate);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, i);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        mDate = (Date) getArguments().getSerializable(ChooserFragment.EXTRA_DATE);

        mCalendar = Calendar.getInstance();
        mCalendar.setTime(mDate);
        int hour = mCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = mCalendar.get(Calendar.MINUTE);

        View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_time, null);

        TimePicker timePicker = (TimePicker) v.findViewById(R.id.dialog_time_timePicker);
        timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(minute);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {

//                Updating the mCalendar
                mCalendar.set(Calendar.HOUR_OF_DAY, hour);
                mCalendar.set(Calendar.MINUTE, minute);

//                Updating the Date from the updated mCalendar
                mDate = mCalendar.getTime();

//                Update the Fragment DATE argument to preserve the value after a rotation
                getArguments().putSerializable(ChooserFragment.EXTRA_DATE, mDate);

            }
        });


        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.time_picker_title)
                .setView(v)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendResult(Activity.RESULT_OK);
                    }
                })
                .create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

//   FAKE CHANGES FOR GIT TEST
    public static TimePickerFragment newInstance(Date date){

        Bundle args = new Bundle();
        args.putSerializable(ChooserFragment.EXTRA_DATE, date);

        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setArguments(args);

        return fragment;
    }

}

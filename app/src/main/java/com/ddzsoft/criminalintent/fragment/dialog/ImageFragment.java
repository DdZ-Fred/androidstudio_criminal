package com.ddzsoft.criminalintent.fragment.dialog;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ddzsoft.criminalintent.utils.PictureUtils;

/**
 * Created by Frédéric on 19/07/2014.
 */
public class ImageFragment extends DialogFragment {

    public static final String EXTRA_IMAGE_PATH = "com.ddzsoft.criminalintent.image_path";
    public static final String EXTRA_IMAGE_ORIENTATION = "com.ddzsoft.criminalintent.image_orientation";

    private ImageView mImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mImageView = new ImageView(getActivity());
        String path = getArguments().getString(EXTRA_IMAGE_PATH);
        int orientation = getArguments().getInt(EXTRA_IMAGE_ORIENTATION);
        BitmapDrawable image = PictureUtils.getScaledDrawable(getActivity(), path);

        if(orientation == Configuration.ORIENTATION_PORTRAIT){

            Matrix mat = new Matrix();
            mat.postRotate(90);

            Bitmap srcBitmap = image.getBitmap();
            Bitmap rotatedBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), mat, true);

            mImageView.setImageBitmap(rotatedBitmap);

        } else {
            mImageView.setImageDrawable(image);
        }

        return mImageView;

    }

    public static ImageFragment newInstance(String imagePath, int imageOrientation){
        Bundle args = new Bundle();
        args.putString(EXTRA_IMAGE_PATH, imagePath);
        args.putInt(EXTRA_IMAGE_ORIENTATION, imageOrientation);

        ImageFragment fragment = new ImageFragment();
        fragment.setArguments(args);
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);

        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PictureUtils.cleanImageView(mImageView);
    }
}

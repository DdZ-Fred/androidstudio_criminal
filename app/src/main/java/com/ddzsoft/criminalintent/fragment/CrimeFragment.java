package com.ddzsoft.criminalintent.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.ddzsoft.criminalintent.R;
import com.ddzsoft.criminalintent.activity.CrimeCameraActivity;
import com.ddzsoft.criminalintent.fragment.dialog.ChooserFragment;
import com.ddzsoft.criminalintent.fragment.dialog.DeleteConfirmationDialog;
import com.ddzsoft.criminalintent.fragment.dialog.ImageFragment;
import com.ddzsoft.criminalintent.model.Crime;
import com.ddzsoft.criminalintent.model.CrimeLab;
import com.ddzsoft.criminalintent.model.Photo;
import com.ddzsoft.criminalintent.utils.PictureUtils;

import org.json.JSONException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Frédéric on 09/07/2014.
 */
public class CrimeFragment extends Fragment {

    private static final String TAG = "CrimeFragment";

    public static final String EXTRA_CRIME_ID = "com.ddzsoft.criminalintent.crime_id";

//  EEEE = Day of week - MMM = Month in year - dd = Day in month - yyyy = Year
    private static final String DATE_FORMAT = "EEEE, MMM dd, yyyy - KK:mm aa";

    private static final String DIALOG_CHOOSER = "chooser";
    private static final String DIALOG_DELETE_CONFIRM = "delete_confirm";
    private static final String DIALOG_IMAGE = "image";

//    Need to be public for ChooserFragment
    public static final int REQUEST_DATE = 0;
    public static final int REQUEST_TIME = 1;
    private static final int REQUEST_PHOTO = 2;
    private static final int REQUEST_CONTACT = 3;
    private static final int REQUEST_CALL = 4;


    private Crime mCrime;
    private EditText mTitleField;
    private Button mDateButton;
    private CheckBox mSolvedCheckBox;
    private ImageButton mPhotoButton;
    private ImageView mPhotoView;
    private Button mSuspectButton;
    private Button mCallSuspectButton;
    private Callbacks mCallbacks;


    public interface Callbacks {
        void onCrimeUpdated(Crime crime);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        To allow/show Up/parent navigation
        setHasOptionsMenu(true);

        UUID crimeId = (UUID) getArguments().getSerializable(EXTRA_CRIME_ID);

        mCrime = CrimeLab.get(getActivity()).getCrime(crimeId);
        Log.d(TAG, "Crime initialized: " + mCrime.getTitle());
    }

    @TargetApi(11)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_crime, parent, false);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            if(NavUtils.getParentActivityName(getActivity()) != null){
                getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
            }

        }

        mTitleField = (EditText) v.findViewById(R.id.crime_title);
        mTitleField.setText(mCrime.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence c, int start, int before, int count) {
                mCrime.setTitle(c.toString());
                mCallbacks.onCrimeUpdated(mCrime);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mDateButton = (Button) v.findViewById(R.id.crime_date);
        updateDate();

        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                ChooserFragment dialog = ChooserFragment.newInstance(mCrime.getDate(), mCrime.getId());
                dialog.show(fm, DIALOG_CHOOSER);
            }
        });


        mSolvedCheckBox = (CheckBox) v.findViewById(R.id.crime_solved);
        mSolvedCheckBox.setChecked(mCrime.isSolved());
        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                mCrime.setSolved(isChecked);
                mCallbacks.onCrimeUpdated(mCrime);
            }
        });


        mPhotoButton = (ImageButton) v.findViewById(R.id.crime_imageButton);
        mPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), CrimeCameraActivity.class);
                startActivityForResult(i, 2);
            }
        });

        mPhotoView = (ImageView) v.findViewById(R.id.crime_imageView);

       if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB){
           registerForContextMenu(mPhotoView);
       } else {

           mPhotoView.setOnLongClickListener(new View.OnLongClickListener() {

               @Override
               public boolean onLongClick(View view) {

                       ActionMode actionMode = getActivity().startActionMode(new ActionMode.Callback() {
                           @Override
                           public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                               MenuInflater inflater = actionMode.getMenuInflater();
                               inflater.inflate(R.menu.fragment_crime_photo_context, menu);
                               return true;
                           }

                           @Override
                           public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                               return false;
                           }

                           @Override
                           public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {

                               switch (menuItem.getItemId()) {
                                   case R.id.menu_item_delete_photo:

//                                      First get the filename to delete it later
                                       String filename = mCrime.getPhoto().getFilename();

//                                      Photo is detached from the ImageView
                                       mPhotoView.setImageDrawable(null);

//                                      Then deleted from the Model
                                       mCrime.setPhoto(null);

//                                      Finally, deleted from disk
                                       deletePhotoFromDisk(filename);

                                       mPhotoView.setEnabled(false);

                                       actionMode.finish();

                                       return true;

                                   default:
                                       return false;

                               }

                           }

                           @Override
                           public void onDestroyActionMode(ActionMode actionMode) {}

                       });

                    return true;

               }
           });
       }


        mPhotoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Photo p = mCrime.getPhoto();

                if(p != null){
                    FragmentManager fm = getActivity().getSupportFragmentManager();
//                    getFileStreamPath returns the absolute path on the filesystem where a file created with openFileOutput(String, int) is stored.
                    String path = getActivity().getFileStreamPath(p.getFilename()).getAbsolutePath();
                    ImageFragment.newInstance(path, p.getOrientation()).show(fm, DIALOG_IMAGE);
                }
            }
        });


//        If the camera is not available, then disable camera functionality
        PackageManager pm = getActivity().getPackageManager();
        boolean hasACamera = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) ||
                pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT) ||
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && Camera.getNumberOfCameras() > 0);
        if(!hasACamera){
            mPhotoButton.setEnabled(false);
        }

        Button reportButton = (Button) v.findViewById(R.id.crime_reportButton);
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, getCrimeReport());
                i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.crime_report_subject));
                i = Intent.createChooser(i, getString(R.string.send_report));
                startActivity(i);
            }
        });

        mSuspectButton = (Button) v.findViewById(R.id.crime_suspectButton);
        mSuspectButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK,
                        ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
            }
        });

        if (mCrime.getSuspect() != null){
            mSuspectButton.setText(mCrime.getSuspect());
        }

        mCallSuspectButton = (Button) v.findViewById(R.id.crime_callSuspectButton);
        mCallSuspectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mCrime.getSuspect() != null) {

                    String phoneNumber = getNumberFromDisplayName(mCrime.getSuspect());
                    Uri number = Uri.parse("tel:" + phoneNumber);


                    TelephonyManager telManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
                    EndCallListener callListener = new EndCallListener();
                    telManager.listen(callListener, PhoneStateListener.LISTEN_CALL_STATE);

                    Intent call = new Intent(Intent.ACTION_DIAL, number);

//                    This flag clears the called app from the activity stack, so users arrive in the expected
//                  place next time this application is restarted.
//                    Check EndCallListener class at the end of the this file!
                    call.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivity(call);

                }


            }
        });


        return v;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch(requestCode){

            case REQUEST_DATE:







                switch (resultCode){
                    case Activity.RESULT_OK:

                        Date dDate = (Date) data.getSerializableExtra(ChooserFragment.EXTRA_DATE);
                        mCrime.setDate(dDate);
                        mCallbacks.onCrimeUpdated(mCrime);
                        updateDate();


                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                    default:
                        break;
                }







                break;
            case REQUEST_TIME:






                switch (resultCode){
                    case Activity.RESULT_OK:

                        Date tDate = (Date) data.getSerializableExtra(ChooserFragment.EXTRA_DATE);
                        mCrime.setDate(tDate);
                        mCallbacks.onCrimeUpdated(mCrime);
                        updateDate();


                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                    default:
                        break;
                }












                break;
            case REQUEST_PHOTO:





                switch (resultCode){
                    case Activity.RESULT_OK:

                        String oldFilename = null;
                        String newFilename = data.getStringExtra(CrimeCameraFragment.EXTRA_PHOTO_FILENAME);
                        int orientation = data.getIntExtra(CrimeCameraFragment.EXTRA_PHOTO_ORIENTATION, Configuration.ORIENTATION_PORTRAIT);
                        if(newFilename != null){
                            Photo p = new Photo(newFilename);
                            p.setOrientation(orientation);

//                            Save the old filename to delete the file later
                            if(mCrime.getPhoto() != null){
                                oldFilename = mCrime.getPhoto().getFilename();
                            }

                            mCrime.setPhoto(p);
                            showPhoto();
                        }

//                        Delete old photo if it exists
                        if(oldFilename != null){
                            deletePhotoFromDisk(oldFilename);
                        }




                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                    default:
                        break;
                }



                break;
            case REQUEST_CONTACT:

                Uri contactUri = data.getData();

//                Specify which fields you want your query to return values for
                String[] queryFields = new String[] {
                        ContactsContract.Contacts.DISPLAY_NAME
                };
//                Perform your query - the contactUri is like a "where" caluse here
                Cursor c = getActivity().getContentResolver().query(contactUri, queryFields, null, null, null);

//                Double-check that you actually got result
                if(c.getCount() != 0){

//                    Pull out the first column of the first row of data - that is your suspect name
                    c.moveToFirst(); /*... first row!*/
                    String suspect = c.getString(0); /*Here, getting the column having index 0 = first column */
                    mCrime.setSuspect(suspect);
                    mCallbacks.onCrimeUpdated(mCrime);
                    mSuspectButton.setText(mCrime.getSuspect());
                    c.close();
                } else {
                    Log.i(TAG, "Cursor empty!");
                    c.close();
                }


                break;
            case REQUEST_CALL:

                Log.i(TAG, "Criminal just ends!");


                break;
            default:
                break;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_crime, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:

//                Navigate to the parent Activity
                navigateToParentActivity();

                return true;

            case R.id.menu_crime_fragment_item_delete:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                DeleteConfirmationDialog dialog = DeleteConfirmationDialog.newInstance(mCrime.getId());
                dialog.show(fm,DIALOG_DELETE_CONFIRM);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

//        We need a Context Menu for only one View, the imageView, so no need to check the viewId
        getActivity().getMenuInflater().inflate(R.menu.fragment_crime_photo_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_item_delete_photo:

//                First get the filename to delete it later
                String filename = mCrime.getPhoto().getFilename();

//                Photo is detached from the ImageView
                mPhotoView.setImageDrawable(null);

//                Then deleted from the Model
                mCrime.setPhoto(null);

//                Finally, deleted from disk
                deletePhotoFromDisk(filename);

                mPhotoView.setEnabled(false);

                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        showPhoto();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            CrimeLab.get(getActivity()).saveCrimesInExternalStorage();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        PictureUtils.cleanImageView(mPhotoView);
    }

    public static CrimeFragment newInstance(UUID crimeId){
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CRIME_ID, crimeId);
        CrimeFragment crimeFragment = new CrimeFragment();
        crimeFragment.setArguments(args);

        return crimeFragment;
    }

    private void updateDate(){
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
        mDateButton.setText(df.format(mCrime.getDate()));
    }

    public void navigateToParentActivity(){
//        Navigate to the parent Activity
        if(NavUtils.getParentActivityName(getActivity()) != null){
            NavUtils.navigateUpFromSameTask(getActivity());
        }
    }

//    (Re)Set the image
    private void showPhoto(){
        Photo p = mCrime.getPhoto();
        BitmapDrawable b = null;
        if(p != null){

//            Enable the view when a photo is available
            mPhotoView.setEnabled(true);

//            Returns the absolute path on the filesystem where a file created with openFileOutput(String, int) is stored.
            String path = getActivity().getFileStreamPath(p.getFilename()).getAbsolutePath();
            b = PictureUtils.getScaledDrawable(getActivity(), path);


            int orientation = p.getOrientation();
            if(orientation == Configuration.ORIENTATION_PORTRAIT){

                Matrix matrix = new Matrix();
                matrix.postRotate(90);

                Bitmap srcBitmap = b.getBitmap();
                Bitmap rotatedBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);

                mPhotoView.setImageBitmap(rotatedBitmap);

            } else {
                mPhotoView.setImageDrawable(b);
            }


        } else {
            mPhotoView.setImageDrawable(b);

            mPhotoView.setEnabled(false);

        }




    }

    public boolean deletePhotoFromDisk(String photoName) {

//           deleteFile: Delete the given private file associated with this Context's application package.
        if (getActivity().deleteFile(photoName)) {
            Log.i(TAG, "Photo " + photoName + " successfully deleted!");
            return true;
        } else {
            Log.i(TAG, "Photo " + photoName + " couldn't be deleted!");
            return false;
        }

    }

    private String getCrimeReport() {
        String solvedString = null;
        if(mCrime.isSolved()) {
            solvedString = getString(R.string.crime_report_solved);
        } else {
            solvedString = getString(R.string.crime_report_unsolved);
        }

//        EEE = 3 first characters of the Day (Ex: Tue)
//        MM = Month in year (number)
//        dd = day in month (number)
        String dateFormat = "EEE, MM dd";
        String dateString = DateFormat.format(dateFormat, mCrime.getDate()).toString();

        String suspect = mCrime.getSuspect();
        if(suspect == null) {
            suspect = getString(R.string.crime_report_no_suspect);
        } else {
            suspect = getString(R.string.crime_report_suspect, suspect);
        }

        String report = getString(R.string.crime_report, mCrime.getTitle(), dateString, solvedString, suspect);

        return report;
    }

    private String getNumberFromDisplayName(String displayName) {

        String phoneNumber = null;

//        Query the Table ContactsContract.CommonDataKinds.Phone using the DISPLAY_NAME
        String[] queryFields =  new String[] {ContactsContract.CommonDataKinds.Phone.NUMBER};

        Cursor c = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                queryFields,
                "DISPLAY_NAME = ?",
                new String[]{displayName},
                null);

        if(c.moveToFirst()) {
            phoneNumber = c.getString(0);
            c.close();
            Log.i(TAG, "Criminal " + displayName + " number is: " + phoneNumber);
            return phoneNumber;
        } else {
            c.close();
            Log.i(TAG, "Finding " + displayName + " number failed!");
            return null;
        }

    }

    private class EndCallListener extends PhoneStateListener {

        private boolean callInitiated = false;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                Log.i(TAG, "RINGING, number: " + incomingNumber);

            }

//            At least one call exists that is dialing, active, or on hold, and no calls are ringing or waiting.
            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                Log.i(TAG, "OFFHOOK");
                callInitiated = true;

            }

//            No activity
            if (TelephonyManager.CALL_STATE_IDLE == state) {

//                if the call has been previously initiated and it's now idle, then return to app.
                if (callInitiated) {

                    Intent i = getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    callInitiated = false;

                }
            }

        }
    }

}

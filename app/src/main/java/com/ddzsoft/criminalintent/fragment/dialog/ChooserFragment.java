package com.ddzsoft.criminalintent.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.ddzsoft.criminalintent.R;
import com.ddzsoft.criminalintent.fragment.CrimeFragment;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Frédéric on 11/07/2014.
 */
public class ChooserFragment extends DialogFragment {

    public static final String TAG = "ChooserFragment";
    public static final String EXTRA_DATE = "com.ddzsoft.criminalintent.date";
    public static final String EXTRA_ID = "com.ddzsoft.criminalintent.id";

    private static final String DIALOG_DATE = "date";
    private static final String DIALOG_TIME = "time";

    private Date mDate;
    private UUID mId;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mDate = (Date) getArguments().getSerializable(EXTRA_DATE);
        mId = (UUID) getArguments().getSerializable(EXTRA_ID);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.chooser_title)
                .setItems(R.array.time_or_date, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int pos) {

                        FragmentManager fm = getActivity().getSupportFragmentManager();

                        switch (pos){
                            case 0:
//                              INDEX 0 = DATE
                                Log.d(TAG, "DatePickerFragment chosen!");
                                DatePickerFragment dateDialog = DatePickerFragment.newInstance(mDate);

                              /*PROBLEM HERE, Fragment are identified by the resource ID of its container
                                but here, the same container is used 3 times for 3 fragments:
                                - 1st time for the Crime clicked, let's say Crime #5
                                - 2nd time for the previous one, so Crime #4
                                - 3rd time for the next one, Crime #5
                                It's possible to find a fragment by TAG. Each Fragment represent a Crime
                                so i'll use their ID as a TAG */

//                                dialog.setTargetFragment(fm.findFragmentById(R.id.viewPager), CrimeFragment.REQUEST_DATE);

                                dateDialog.setTargetFragment(fm.findFragmentByTag(mId.toString()), CrimeFragment.REQUEST_DATE);

                                dateDialog.show(fm,DIALOG_DATE);

                                break;

                            case 1:
//                              INDEX 1 = TIME
                                Log.d(TAG, "TimePickerFragment chosen!");

                                TimePickerFragment timeDialog = TimePickerFragment.newInstance(mDate);

                                timeDialog.setTargetFragment(fm.findFragmentByTag(mId.toString()),CrimeFragment.REQUEST_TIME);
                                timeDialog.show(fm, DIALOG_TIME);

                                break;


                            default:
                                break;
                        }
                    }
                });

        return builder.create();
    }


    public static ChooserFragment newInstance(Date date, UUID id){
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DATE, date);
        args.putSerializable(EXTRA_ID, id);

        ChooserFragment fragment = new ChooserFragment();
        fragment.setArguments(args);

        return fragment;
    }
}

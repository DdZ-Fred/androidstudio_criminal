package com.ddzsoft.criminalintent.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Window;
import android.view.WindowManager;

import com.ddzsoft.criminalintent.fragment.CrimeCameraFragment;

/**
 * Created by Frédéric on 18/07/2014.
 */
public class CrimeCameraActivity extends SingleFragmentActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        Hide the windows title
        requestWindowFeature(Window.FEATURE_NO_TITLE);

//        Hide the status bar and other OS-level chrome
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
    }

    @Override
    protected Fragment createFragment() {
        return new CrimeCameraFragment();
    }

}

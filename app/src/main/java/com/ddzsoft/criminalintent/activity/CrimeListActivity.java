package com.ddzsoft.criminalintent.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.ddzsoft.criminalintent.R;
import com.ddzsoft.criminalintent.fragment.CrimeFragment;
import com.ddzsoft.criminalintent.fragment.CrimeListFragment;
import com.ddzsoft.criminalintent.model.Crime;

/**
 * Created by Frédéric on 10/07/2014.
 */
public class CrimeListActivity extends SingleFragmentActivity implements CrimeListFragment.Callbacks, CrimeFragment.Callbacks
{

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment() {
        return new CrimeListFragment();
    }

    @Override
    public void onCrimeSelected(Crime crime) {

/*        If view =  null, then the view doesn't exist! means that we're in "Phone" mode!
        and that the view is in an other Activity!*/
        if(findViewById(R.id.detailFragmentContainer) == null ){
//            Start an instance of CrimePagerActivity
            Intent i = new Intent(this, CrimePagerActivity.class);
            i.putExtra(CrimeFragment.EXTRA_CRIME_ID, crime.getId());
            startActivity(i);
        } else {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            Fragment oldDetail = fm.findFragmentById(R.id.detailFragmentContainer);
            Fragment newDetail = CrimeFragment.newInstance(crime.getId());

//            We Check if there was already a fragment
            if(oldDetail != null) {
                ft.remove(oldDetail);
            }

//            I add a TAG, so i can retrieve the good CrimeFragment when updating the date/time
            ft.add(R.id.detailFragmentContainer, newDetail, crime.getId().toString() );
            ft.commit();

        }
    }

    @Override
    public void onCrimeUpdated(Crime crime) {
        FragmentManager fm = getSupportFragmentManager();
        CrimeListFragment listFragment = (CrimeListFragment) fm.findFragmentById(R.id.fragmentContainer);
        listFragment.updateUI();
    }
}

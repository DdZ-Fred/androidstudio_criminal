package com.ddzsoft.criminalintent.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.ddzsoft.criminalintent.R;
import com.ddzsoft.criminalintent.fragment.CrimeFragment;
import com.ddzsoft.criminalintent.model.Crime;
import com.ddzsoft.criminalintent.model.CrimeLab;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Frédéric on 11/07/2014.
 */
public class CrimePagerActivity extends FragmentActivity implements CrimeFragment.Callbacks {

    private static final String TAG = "CrimePagerActivity";

    private ViewPager mViewPager;
    private ArrayList<Crime> mCrimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate() launched!");

        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.viewPager);
        setContentView(mViewPager);

        mCrimes = CrimeLab.get(this).getCrimes();

        final FragmentManager fm = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public Fragment getItem(int i) {
                Crime crime = mCrimes.get(i);
                CrimeFragment crimeFrag = CrimeFragment.newInstance(crime.getId());
                fm.beginTransaction().replace(R.id.viewPager, crimeFrag, crime.getId().toString());

                return crimeFrag;
            }

            @Override
            public int getCount() {
                return mCrimes.size();
            }
        });

        UUID crimeId = (UUID) getIntent().getSerializableExtra(CrimeFragment.EXTRA_CRIME_ID);

        for(int i = 0; i < mCrimes.size(); i++){
            if(mCrimes.get(i).getId().equals(crimeId)){
                mViewPager.setCurrentItem(i);
                if(mCrimes.get(i).getTitle() != null){
                    setTitle(mCrimes.get(i).getTitle());
                }
                break;
            }

        }
        Log.d(TAG, "Break passed!!");

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float posOffset, int posOffsetPixels) {}

            @Override
            public void onPageSelected(int i) {
                Crime crime = mCrimes.get(i);
                if(crime.getTitle() != null){
                    setTitle(crime.getTitle());
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() launched!");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() launched!");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() launched!");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() launched!");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() launched!");
    }

    @Override
    public void onCrimeUpdated(Crime crime) {

    }
}

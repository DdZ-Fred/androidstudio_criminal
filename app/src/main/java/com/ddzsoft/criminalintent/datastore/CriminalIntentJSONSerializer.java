package com.ddzsoft.criminalintent.datastore;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.ddzsoft.criminalintent.model.Crime;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by Frédéric on 16/07/2014.
 */
public class CriminalIntentJSONSerializer {

    private static final String TAG = "CriminalIntentJSONSerializer";

    private Context mContext;
    private String mFileName;

    public CriminalIntentJSONSerializer(Context c, String f){
        mContext = c;
        mFileName = f;
    }

    public void saveCrimes(ArrayList<Crime> crimes) throws IOException, JSONException {

//        Build an array in JSON
        JSONArray array = new JSONArray();
        for(Crime c: crimes){
            array.put(c.toJSON());
        }

//        Write the file to disk
        Writer writer = null;
        try {
            OutputStream out = mContext.openFileOutput(mFileName, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        } finally {
            if(writer != null){
                writer.close();
            }
        }

    }

    public void saveCrimesInExternalStorage(ArrayList<Crime> crimes) throws JSONException, IOException {

        if (isExternalStorageWritable()) {
//            Returns the root directory for your app's private directory on the external storage.
            File dir = mContext.getExternalFilesDir(null);

//        Returns false if already exists or if it fails
            dir.mkdirs();

            File file = new File(dir, mFileName);

            JSONArray array = new JSONArray();
            for (Crime c : crimes) {
                array.put(c.toJSON());
            }

//        If the dir creation hasn't failed or if it was already present
            if (dir.isDirectory()) {

                Writer writer = null;
                OutputStream out = null;
                try {
/*                Constructs a new FileOutputStream that writes to file. The file will be truncated
                if it exists, and created if it doesn't exist.*/
                    out = new BufferedOutputStream(new FileOutputStream(file));

/*                Constructs a new OutputStreamWriter using out as the target stream to write converted
                characters to. The default character encoding is used.*/
                    writer = new OutputStreamWriter(out);

                    writer.write(array.toString());

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } finally {
                    if (writer != null) {
                        writer.close();
                    }
                    if (out != null) {
                        out.close();
                    }
                }
            } else {
                Log.d(TAG, "Error during directories creation!");
            }

        } else {
            Log.d(TAG, "SD card not available");
        }

    }

    public ArrayList<Crime> loadCrimes() throws IOException, JSONException {
        ArrayList<Crime> crimes = new ArrayList<Crime>();

        BufferedReader reader = null;
        try {
//            Open and read the file into a StringBuilder
            InputStream in = mContext.openFileInput(mFileName);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null){
//                Line breaks are omitted and irrelevant
                jsonString.append(line);
            }
//            Parse the JSON using JSONTokener
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
//            Build the array of crimes from JSONObjects
            for(int i = 0; i < array.length(); i++){
                crimes.add(new Crime(array.getJSONObject(i)));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (reader != null){
                reader.close();
            }
        }

        return crimes;
    }

    public ArrayList<Crime> loadCrimesFromExternalStorage() throws IOException, JSONException {

        ArrayList<Crime> crimes = new ArrayList<Crime>();

        if(isExternalStorageReadable()){

            File file = new File(mContext.getExternalFilesDir(null), mFileName);

//            If the file is present on the file system!
            if(file.isFile()){

                BufferedReader reader = null;
                InputStream in = null;
                try {
                    in = new BufferedInputStream(new FileInputStream(file));
                    reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder jsonString = new StringBuilder();
                    String line = null;
                    while((line = reader.readLine()) != null){
//                      Line breaks are omitted and irrelevant
                        jsonString.append(line);
                    }

//                  Parse the JSON using JSONTokener
                    JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
//                  Build the array of crimes from JSONObjects
                    for(int i = 0; i < array.length(); i++){
                        crimes.add(new Crime(array.getJSONObject(i)));
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } finally {
                    if(reader != null){
                        reader.close();
                    }
                    if(in != null){
                        in.close();
                    }
                }

            } else {
                Log.d(TAG, "File doesn't exist");
            }

        } else {
            Log.d(TAG, "Can't read the SD card");
        }

        return crimes;
    }

    public boolean isExternalStorageWritable(){
//        Storage state if the media is present and mounted at its mount point with read/write access
        String state = Environment.getExternalStorageState();
        if(state.equals(Environment.MEDIA_MOUNTED)){
            return true;
        }
        return false;
    }

    public boolean isExternalStorageReadable(){
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)){
            return true;
        }
        return false;
    }

}

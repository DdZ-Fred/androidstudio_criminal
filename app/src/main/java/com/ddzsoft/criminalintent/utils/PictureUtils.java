package com.ddzsoft.criminalintent.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.Display;
import android.widget.ImageView;

/**
 * Created by Frédéric on 19/07/2014.
 */
public class PictureUtils {

/*
    Get a BitmapDrawable from a local file that is scaled down to fit the current Window size
*/
    @SuppressWarnings("deprecation")
    public static BitmapDrawable getScaledDrawable(Activity a, String path) {
        Display display = a.getWindowManager().getDefaultDisplay();
        float destWidth = display.getWidth();
        float destHeight = display.getHeight();

//        Read in the dimensions of the image on disk
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        float srcWidth = options.outWidth;
        float srcHeight = options.outWidth;

        int inSampleSize = 1;
        if (srcHeight > destHeight || srcWidth > destWidth){
//            inSample will obviously be > 1.
            if(srcWidth > srcHeight){
                inSampleSize = Math.round(srcHeight / destHeight);
            } else {
                inSampleSize = Math.round(srcWidth / destWidth);
            }
        }
/*        If set to a value > 1, requests the decoder to subsample the original image,
        returning a smaller image to save memory. The sample size is the number of
        pixels in either dimension that correspond to a single pixel in the decoded bitmap*/
        options = new BitmapFactory.Options();
        options.inSampleSize = inSampleSize;

        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        return new BitmapDrawable(a.getResources(), bitmap);
    }

    public static void cleanImageView(ImageView imageView){
        if(imageView.getDrawable() instanceof BitmapDrawable) {

//          Clean up the view's image for the sake of memory
            BitmapDrawable b = (BitmapDrawable) imageView.getDrawable();
            b.getBitmap().recycle();
            imageView.setImageDrawable(null);
        }
    }

}
